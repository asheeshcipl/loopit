@extends('layout')
  
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('A list of cars in stock') }}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
			<div class="card"><div class="card-header"><br/></div></div>
			<div class="card">
			<div class="card-body">
				<table class="table" id="table_id">
					<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Name</th>
						  <th scope="col">Detail</th>
						  <th scope="col">Quantity Available</th>
						</tr>
					</thead>
					<tbody id="productslist">

					</tbody>
				</table>
			</div>  
			</div>  
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script>
$.ajax({
	url: "{{url('api/products')}}",
	type: "GET",
	data: {
		"_token": "{{ csrf_token() }}"			
	},
	success: function (response) {
		var products = response.success;
		$.each(products, function() {
			$.each(this, function(k, v) {
				var html = '';
				 html = '<tr>';
				 html += '<th scope="row">'+v.id+'</th>';
				 html += '<td>'+v.name+'</td>';
				 html += '<td>'+v.detail+'</td>';
				 html += '<td>'+v.quantity+'</td>';				
				 html += '</tr>';				
				$('#productslist').append(html);	
			})	
		});
		$('#table_id').DataTable();
	},
	error: function () {
		// swal("Error", "Unable to bring up the dialog.", "error");
	}
});

</script>
@endsection