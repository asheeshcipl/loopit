@extends('layout')
  
@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Login</div>
                  <div class="card-body">
  
                      <form action="#" method="POST">
                          @csrf
                          <div class="form-group row">
                              <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                              <div class="col-md-6">
                                  <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                                  @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                              <div class="col-md-6">
                                  <input type="password" id="password" class="form-control" name="password" required>
                                  @if ($errors->has('password'))
                                      <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                              </div>
                          </div>  
                         
  
                          <div class="col-md-6 offset-md-4">
                              <button type="button" id="submitButton" class="btn btn-primary">
                                  Login
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
$('#submitButton').on('click', function (e) {
	e.preventDefault(); // Want to stay on the page, not go through normal form  
	// Might be easier to make this a NON submit button - then we can use the rest below and not have to js submit()
	// Grab any extra info via data.
	var email_address = $('#email_address').val();
	var password = $('#password').val();
	$.ajax({
		url: "{{url('api/login')}}",
		type: "POST",
		data: {
			"_token": "{{ csrf_token() }}",
			"email": email_address,
			"password": password
		},
		success: function (message) {
			var loc = window.location;
			window.location.href  = "http://localhost:8012/loopitproject/public/dashboard";
			// Or, if you want a better looking alert, you can use a library like SWAL:
		   // swal("Success!", "New user created with a name of: "+name, "success");
		},
		error: function () {
			// swal("Error", "Unable to bring up the dialog.", "error");
		}
	});
});
</script>
@endsection
