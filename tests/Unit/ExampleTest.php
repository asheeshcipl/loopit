<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
	
	protected function tokensMatch($request)
	{
	   // Don't validate CSRF when testing.
	   if(env('APP_ENV') === 'laraveltesting') {
		   return true;
	   }
	   return parent::tokensMatch($request);
	}
	
	public function testHasItemInBasket()
	{
		$basket = new Basket(['item_one', 'item_two', 'item_three']);
		$this->assertTrue($basket->has('item_one'));
		$this->assertFalse($basket->has('item_four'));
	}
	public function testTakeOneFromTheBasket()
	{
		$basket = new Basket(['item_three']);
		$this->assertEquals('item_three', $basket->takeOne());
		$this->assertNull($basket->takeOne());
	}
}
